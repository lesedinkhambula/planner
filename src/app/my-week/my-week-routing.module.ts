import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyWeekComponent } from './my-week.component';


const routes: Routes = [
  {
    path: '',
    component: MyWeekComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyWeekRoutingModule { }