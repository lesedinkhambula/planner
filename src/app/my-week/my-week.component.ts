import { Component, OnInit } from '@angular/core';
import { _MatTabBodyBase } from '@angular/material/tabs';

@Component({
  selector: 'app-my-week',
  templateUrl: './my-week.component.html',
  styleUrls: ['./my-week.component.scss']
})
export class MyWeekComponent implements OnInit {

  list = [1,2,3,4,5,6,7];
  today = new Date();
  nextWeek: Date[];
  constructor() { }

  ngOnInit(): void {
    this.getMyWeek();
  }

  getMyWeek(): void{

    const week = [];
    week.push(this.addDay(this.today,0));

    console.log(this.today)
    week.push(this.addDay(this.today,1));
    console.log(this.today)
    week.push(this.addDay(this.today,2));
    console.log(this.today)
    week.push(this.addDay(this.today,3));
    console.log(this.today)
    week.push(this.addDay(this.today,4));
    console.log(this.today)
    week.push(this.addDay(this.today,5));
    console.log(this.today)
    week.push(this.addDay(this.today,6));
    console.log(this.today)

    this.nextWeek = week;

  }

  addDay(oldDate: Date, days: number): Date {
    oldDate.setDate(oldDate.getDate() + days);

    return oldDate;
  }

}
