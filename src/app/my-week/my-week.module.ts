import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyWeekComponent } from './my-week.component';
import { MyWeekRoutingModule } from './my-week-routing.module';
import { AngularMaterialModule } from '../angular-material.module';


@NgModule({
  declarations: [
    MyWeekComponent
  ],
  imports: [
    CommonModule,
    MyWeekRoutingModule,
    AngularMaterialModule
  ]
})
export class MyWeekModule { }
